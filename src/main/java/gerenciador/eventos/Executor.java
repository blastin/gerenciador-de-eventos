package gerenciador.eventos;

/**
 * Possibilita que Eventos sejam executados
 *
 * @apiNote Eventos que precisam ser agendados seram executados por um executor. Normalmente
 * são específicos de tecnologia
 */
public interface Executor {

    void aplicar(final Agenda agenda, final Runnable runnable);

    void aplicar(final Runnable runnable);

}
