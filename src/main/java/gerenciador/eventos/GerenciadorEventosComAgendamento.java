package gerenciador.eventos;

final class GerenciadorEventosComAgendamento implements GerenciadorEventos {

    GerenciadorEventosComAgendamento(final Executor executor, final Agenda agenda) {
        this.executor = executor;
        this.agenda = agenda;
    }

    private final Executor executor;
    private final Agenda agenda;

    @Override
    public <T> GerenciadorEventos disparar(final T objeto, final EventoComAgendamento<T> eventoComAgendamento) {
        executor.aplicar(agenda, () -> eventoComAgendamento.executar(objeto, t -> disparar(t, eventoComAgendamento)));
        return this;
    }

    @Override
    public <T> GerenciadorEventos disparar(final T objeto, final EventoSemAgendamento<T> eventoSemAgendamento) {
        executor.aplicar(agenda, () -> eventoSemAgendamento.executar(objeto));
        return this;
    }

}