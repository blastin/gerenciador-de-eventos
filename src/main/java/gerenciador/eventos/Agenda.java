package gerenciador.eventos;

import java.util.concurrent.TimeUnit;

/**
 * Agenda é uma interface necessária para o agendamento de eventos a serem encaminhadas para um executor
 */
public interface Agenda {

    long obterTempo();

    TimeUnit obterUnidadeTempo();

}
