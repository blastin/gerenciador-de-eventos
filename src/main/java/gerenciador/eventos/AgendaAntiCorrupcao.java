package gerenciador.eventos;

import java.util.concurrent.TimeUnit;

final class AgendaAntiCorrupcao implements Agenda {

    AgendaAntiCorrupcao(final Agenda agenda) {
        this.agenda = agenda;
    }

    private final Agenda agenda;

    @Override
    public long obterTempo() {
        return agenda.obterTempo();
    }

    @Override
    public TimeUnit obterUnidadeTempo() {

        final TimeUnit timeUnit = agenda.obterUnidadeTempo();

        if (timeUnit == null) return TimeUnit.MILLISECONDS;

        return timeUnit;

    }

}
