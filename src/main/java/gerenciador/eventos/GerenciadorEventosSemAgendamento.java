package gerenciador.eventos;

final class GerenciadorEventosSemAgendamento implements GerenciadorEventos {

    GerenciadorEventosSemAgendamento(final Executor executor) {
        this.executor = executor;
    }

    private final Executor executor;

    @Override
    public <T> GerenciadorEventos disparar(final T objeto, final EventoComAgendamento<T> eventoComAgendamento) {
        return this;
    }

    @Override
    public <T> GerenciadorEventos disparar(final T objeto, final EventoSemAgendamento<T> eventoSemAgendamento) {
        executor.aplicar(() -> eventoSemAgendamento.executar(objeto));
        return this;
    }

}
