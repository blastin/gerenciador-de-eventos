package gerenciador.eventos;

/**
 * Gerencia eventos disparados. Eventos cuja entrada deve ser do mesmo tipo do objeto
 */
public interface GerenciadorEventos {

    /**
     * @param objeto            entrada do evento
     * @param eventoComAgendamento evento agendável e com feedback
     * @param <T>               tipo generico, qualquer coisa desde que o evento seja o mesmo tipo
     * @apiNote Dispara eventos com feedback
     */
    <T> GerenciadorEventos disparar(final T objeto, final EventoComAgendamento<T> eventoComAgendamento);

    /**
     * @param objeto entrada do evento
     * @param eventoSemAgendamento evento sem agendamento e sem feedback
     * @param <T>    tipo generico, qualquer coisa desde que o evento seja o mesmo tipo
     * @apiNote Dispara eventos
     */
    <T> GerenciadorEventos disparar(final T objeto, final EventoSemAgendamento<T> eventoSemAgendamento);

}
