package gerenciador.eventos;

/**
 * @apiNote Caso uma agenda não seja informada, não ocorrerá um disparo de evento por feedback
 */
public final class Gerenciadores {

    public static Gerenciadores construtor() {
        return new Gerenciadores();
    }

    private Gerenciadores() {
        agenda = null;
        executor = new ExecutorNulo();
    }

    private Agenda agenda;

    private Executor executor;

    public Gerenciadores agendavel(final Agenda agenda) {
        this.agenda = agenda;
        return this;
    }

    public Gerenciadores executavelPor(final Executor executor) {
        if (executor != null) this.executor = executor;
        return this;
    }

    public GerenciadorEventos construir() {

        if (agenda == null) return new GerenciadorEventosSemAgendamento(executor);

        final AgendaAntiCorrupcao agendaAntiCorrupcao = new AgendaAntiCorrupcao(agenda);

        return new GerenciadorEventosComAgendamento(executor, agendaAntiCorrupcao);

    }

    private static class ExecutorNulo implements Executor {

        @Override
        public void aplicar(final Agenda agenda, final Runnable runnable) {
            aplicar(runnable);
        }

        @Override
        public void aplicar(final Runnable runnable) {
            runnable.run();
        }

    }

}
