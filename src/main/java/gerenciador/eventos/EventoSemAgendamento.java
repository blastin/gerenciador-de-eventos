package gerenciador.eventos;

/**
 * Representa um comportamento abstrato para lidar com eventos com feedback
 *
 * @param <T> objeto de qualquer tipo
 * @apiNote Evento dispara qualquer atividade cuja entrada e generica e lida com feedback
 */
@FunctionalInterface
public interface EventoSemAgendamento<T> {

    void executar(final T objeto);

}
