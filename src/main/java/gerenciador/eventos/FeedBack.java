package gerenciador.eventos;

/**
 * Caso um evento não possa completar sua computação, ele será reprocessado pelo
 * gerenciador de eventos
 *
 * @param <T> qualquer objeto do tipo T
 */
public interface FeedBack<T> {

    void capturar(final T t);

}
