package gerenciador.eventos;

import java.util.concurrent.TimeUnit;

final class AgendaParaTeste implements Agenda {

    AgendaParaTeste(final long tempo, final TimeUnit unidade) {
        this.tempo = tempo;
        this.unidade = unidade;
    }

    private final long tempo;
    private final TimeUnit unidade;

    @Override
    public long obterTempo() {
        return tempo;
    }

    @Override
    public TimeUnit obterUnidadeTempo() {
        return unidade;
    }

}
