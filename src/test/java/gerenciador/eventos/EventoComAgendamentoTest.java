package gerenciador.eventos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class EventoComAgendamentoTest {

    @Test
    @DisplayName("Disparando evento que retorna com feedback")
    void dispararEventoComFeedBack() {

        final AtomicInteger valor = new AtomicInteger(0);

        final EventoComAgendamento<Integer> eventoComFeedbackSomaPorAgendamento = ((objeto, feedback) -> feedback.capturar(objeto));

        eventoComFeedbackSomaPorAgendamento.executar(10, objetoParaReprocesso -> valor.set(objetoParaReprocesso + 10));

        Assertions.assertEquals(20, valor.get());

    }

}
