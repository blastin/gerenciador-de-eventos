package gerenciador.eventos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class EventoSemAgendamentoTest {

    @Test
    @DisplayName("Disparando um evento de negação booleana. Caso o evento seja disparado, altere-o")
    void dispararEventoDeNegacaoBooleana() {

        final AtomicBoolean boolNegado = new AtomicBoolean(false);

        final EventoSemAgendamento<Boolean> eventoSemAgendamento = bool -> boolNegado.set(!bool);

        final boolean entradaEvento = false;

        eventoSemAgendamento.executar(entradaEvento);

        Assertions.assertTrue(boolNegado.get());

    }

    @Test
    @DisplayName("Disparando evento que acrescenta 100 em um bigdecimal")
    void dispararEventoAcrescenta100EmBigDecimal() {

        final AtomicInteger custo = new AtomicInteger(100);

        final EventoSemAgendamento<Integer> eventoSemAgendamento = valor -> custo.accumulateAndGet(valor, Integer::sum);

        eventoSemAgendamento.executar(100);

        Assertions.assertEquals(200, custo.get());

    }

    @Test
    @DisplayName("Disparando evento que imprime algo na tela")
    void dispararEventoPrintf() {

        final EventoSemAgendamento<String> eventoSemFeedbackComAgendamentoImprimir = System.out::println;

        Assertions.assertDoesNotThrow(() -> eventoSemFeedbackComAgendamentoImprimir.executar("Olá mundo"));

    }

}