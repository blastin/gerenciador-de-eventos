package gerenciador.eventos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class GerenciadorEventosTest {

    @Test
    @DisplayName(" Gerenciador que dispara um evento sem utilização de concorrência")
    void criarGerenciadorDeEventosSimples() {

        final GerenciadorEventos gerenciadorEventos = novoGerenciadorEventosSemAgenda();

        gerenciadorEventos.disparar("Olá mundo", System.out::println);

    }

    @Test
    @DisplayName(" Gerenciador que dispara um evento que um valor com outro")
    void criarGerenciadorDeEventosSimplesQueRealizaUmaSoma() {

        final GerenciadorEventos gerenciadorEventos =
                Gerenciadores
                        .construtor()
                        .agendavel(new AgendaParaTeste(30, TimeUnit.MILLISECONDS))
                        .construir();

        final AtomicInteger valor = new AtomicInteger(1000);

        gerenciadorEventos.disparar(100, objeto -> valor.accumulateAndGet(objeto, Integer::sum));

        Assertions.assertEquals(1100, valor.get());

    }

    @Test
    @DisplayName(" Gerenciador que dispara três evento")
    void criarGerenciadorDeEventosSimplesQueDisparaTresEventos() {

        final GerenciadorEventos gerenciadorEventos = novoGerenciadorEventosSemAgenda();

        final AtomicInteger valor = new AtomicInteger(1000);

        gerenciadorEventos
                .disparar("Ola", System.out::println)
                .disparar(" Mundo ", System.out::println)
                .disparar(123, System.out::println)
                .disparar(-100, objeto -> valor.accumulateAndGet(objeto, Integer::sum));

        Assertions.assertEquals(900, valor.get());

    }

    @Test
    @DisplayName("gerenciador de eventos deve lidar com feedback de evento para reexecuta-lo")
    void criarGerenciadorDeEventosParaEventosComFeedback() {

        final AtomicInteger valor = new AtomicInteger(0);

        final AtomicBoolean statusFeedback = new AtomicBoolean(true);

        final GerenciadorEventos gerenciadorEventos = Gerenciadores
                .construtor()
                .agendavel(new AgendaParaTeste(1, TimeUnit.SECONDS))
                .construir();

        gerenciadorEventos
                .disparar(false, (objeto, feedback) -> {
                    if (statusFeedback.getAndSet(objeto)) {
                        valor.set(5);
                        feedback.capturar(objeto);
                    } else {
                        valor.accumulateAndGet(10, Integer::sum);
                    }
                });

        Assertions.assertEquals(15, valor.get());

    }

    private static GerenciadorEventos novoGerenciadorEventosSemAgenda() {
        return Gerenciadores
                .construtor()
                .construir();
    }

}