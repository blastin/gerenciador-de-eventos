package gerenciador.eventos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

class ExecutorTest {

    @Test
    @DisplayName("Criar um executor simples para alterar um valor atomico")
    void executorSimples() {

        final AtomicBoolean executavel = new AtomicBoolean(false);

        final Executor executor = new ExecutorParaTeste();

        executor.aplicar(() -> executavel.set(true));

        Assertions.assertTrue(executavel.get());

    }

    @Test
    @DisplayName("Criar um executor para executar algo após 1 segundo")
    void executorSimplesParaExecutarAlgoApos1Segundo() {

        final Agenda agenda = new AgendaParaTeste(1, TimeUnit.SECONDS);

        final AtomicBoolean executavel = new AtomicBoolean(false);

        final Executor executor = new ExecutorParaTeste();

        executor.aplicar(agenda, () -> executavel.set(true));

        Assertions.assertTrue(executavel.get());

    }

}