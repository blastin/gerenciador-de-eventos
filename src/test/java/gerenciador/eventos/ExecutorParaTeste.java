package gerenciador.eventos;

/**
 * Executor Simples para teste. Utiliza Thread sleep para bloquear inicio de execução de um runnable
 */
final class ExecutorParaTeste implements Executor {

    @Override
    public void aplicar(final Agenda agendaExecutavel, final Runnable runnable) {

        try {

            agendaExecutavel
                    .obterUnidadeTempo()
                    .sleep(agendaExecutavel.obterTempo());

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        aplicar(runnable);

    }

    @Override
    public void aplicar(final Runnable runnable) {
        runnable.run();
    }

}
