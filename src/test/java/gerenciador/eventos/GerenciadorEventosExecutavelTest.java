package gerenciador.eventos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

class GerenciadorEventosExecutavelTest {

    private static final Executor EXECUTOR_PARA_TESTE = new ExecutorParaTeste();

    @Test
    @DisplayName("Gerenciando um evento a partir de um executor. Sem agendamento")
    void gerenciadorExecutavelSimples() {


        final GerenciadorEventos gerenciadorEventos =
                Gerenciadores
                        .construtor()
                        .executavelPor(EXECUTOR_PARA_TESTE)
                        .construir();

        final AtomicBoolean mudancaAtomica = new AtomicBoolean(true);

        gerenciadorEventos
                .disparar(false, mudancaAtomica::set);

        Assertions.assertFalse(mudancaAtomica.get());

    }

    @Test
    @Timeout(value = 30, unit = TimeUnit.MILLISECONDS)
    @DisplayName("Gerenciando um evento a partir de um executor que não agenda")
    void gerenciadorExecutavelSemAgendamento() {

        final GerenciadorEventos gerenciadorEventos =
                Gerenciadores
                        .construtor()
                        .executavelPor(EXECUTOR_PARA_TESTE)
                        .construir();

        final AtomicBoolean mudancaAtomica = new AtomicBoolean(true);

        gerenciadorEventos
                .disparar(false, mudancaAtomica::set);

        Assertions.assertFalse(mudancaAtomica.get());

    }

    @Test
    @DisplayName("Gerenciando um evento para ser executado após 2 segundos")
    void gerenciadorExecutavelComAgendamentoDeEventoApos2Segundos() {

        final Agenda agenda = new AgendaParaTeste(2, TimeUnit.SECONDS);

        final GerenciadorEventos gerenciadorEventos =
                Gerenciadores
                        .construtor()
                        .executavelPor(EXECUTOR_PARA_TESTE)
                        .agendavel(agenda)
                        .construir();

        gerenciadorEventos
                .disparar("Já se foi disco voador", (objeto, feedback) -> System.out.println(objeto));

    }

    @Test
    @DisplayName("Gerenciando um evento para ser executado após 1 segundos , porém com timeunit nulo")
    void gerenciadorExecutavelComTimeUnitNulo() {

        final Agenda agenda = new AgendaParaTeste(1, null);

        final GerenciadorEventos gerenciadorEventos =
                Gerenciadores
                        .construtor()
                        .executavelPor(EXECUTOR_PARA_TESTE)
                        .agendavel(agenda)
                        .construir();

        gerenciadorEventos
                .disparar("Já se foi disco voador", (objeto, feedback) -> System.out.println(objeto));

    }

}
